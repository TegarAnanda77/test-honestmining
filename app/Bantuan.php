<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bantuan
 *
 * @property int $id
 * @property int|null $asker_user_id
 * @property string|null $tanggal
 * @property string|null $judul
 * @property float|null $jumlah_bantuan
 * @property string|null $detail
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereAskerUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereJudul($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereJumlahBantuan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereTanggal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bantuan whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\UserAsker|null $userAsker
 */
class Bantuan extends Model
{
    protected $table = 'bantuan';

    public function userAsker()
    {
        return $this->belongsTo(UserAsker::class, 'asker_user_id', 'id');
    }
}
