<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\BantuanGiver
 *
 * @property int $id
 * @property int|null $bantuan_id
 * @property int|null $giver_user_id
 * @property string|null $tanggal_dibantu
 * @property float|null $nilai_bantuan
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereBantuanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereGiverUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereNilaiBantuan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereTanggalDibantu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BantuanGiver whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BantuanGiver extends Model
{
    protected $table = 'bantuan_giver';
}
