<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserAsker
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $password
 * @property int|null $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserAsker whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class UserAsker extends Model
{
    protected $table = 'user_asker';

    protected $fillable = [
        'name', 'email', 'password',
    ];
}
