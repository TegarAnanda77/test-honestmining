<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\UserGiver
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property string|null $password
 * @property int|null $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $remember_token
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereRememberToken($value)
 * @property int $saldo
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserGiver whereSaldo($value)
 */
class UserGiver extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $table = 'user_giver';
}
