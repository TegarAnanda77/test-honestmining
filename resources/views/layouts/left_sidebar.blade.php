
<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Oreo</a></li>
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Blog"><i class="zmdi zmdi-blogger m-r-5"></i>Blog</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN</li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
                        <ul class="ml-menu">
                            <li><a href="index.html">Main</a> </li>
                            <li><a href="dashboard-rtl.html">RTL</a></li>
                            <li><a href="index2.html">Horizontal</a></li>
                            <li><a href="ec-dashboard.html">Ecommerce</a></li>
                            <li><a href="blog-dashboard.html">Blog</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-apps"></i><span>App</span> </a>
                        <ul class="ml-menu">
                            <li><a href="mail-inbox.html">Inbox</a></li>
                            <li><a href="chat.html">Chat</a></li>
                            <li><a href="events.html">Calendar</a></li>
                            <li><a href="file-dashboard.html">File Manager</a></li>
                            <li><a href="contact.html">Contact list</a></li>
                            <li><a href="blog-dashboard.html">Blog</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-shopping-cart"></i><span>Ecommerce</span> </a>
                        <ul class="ml-menu">
                            <li> <a href="ec-dashboard.html">Dashboard</a></li>
                            <li> <a href="ec-product.html">Product</a></li>
                            <li> <a href="ec-product-List.html">Product List</a></li>
                            <li> <a href="ec-product-detail.html">Product detail</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-swap-alt"></i><span>User Interface (UI)</span> </a>
                        <ul class="ml-menu">
                            <li> <a href="ui_kit.html">UI KIT</a> </li>
                            <li> <a href="alerts.html">Alerts</a> </li>
                            <li> <a href="collapse.html">Collapse</a> </li>
                            <li> <a href="colors.html">Colors</a> </li>
                            <li> <a href="dialogs.html">Dialogs</a> </li>
                            <li> <a href="icons.html">Icons</a> </li>
                            <li> <a href="list-group.html">List Group</a> </li>
                            <li> <a href="media-object.html">Media Object</a> </li>
                            <li> <a href="modals.html">Modals</a> </li>
                            <li> <a href="notifications.html">Notifications</a></li>
                            <li> <a href="progressbars.html">Progress Bars</a></li>
                            <li> <a href="range-sliders.html">Range Sliders</a></li>
                            <li> <a href="sortable-nestable.html">Sortable & Nestable</a></li>
                            <li> <a href="tabs.html">Tabs</a></li>
                            <li> <a href="waves.html">Waves</a></li>
                        </ul>
                    </li>
                    <li class="header">FORMS, CHARTS, TABLES</li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-assignment"></i><span>Forms</span> </a>
                        <ul class="ml-menu">
                            <li><a href="basic-form-elements.html">Basic Elements</a> </li>
                            <li><a href="advanced-form-elements.html">Advanced Elements</a> </li>
                            <li><a href="form-examples.html">Form Examples</a> </li>
                            <li><a href="form-validation.html">Form Validation</a> </li>
                            <li><a href="form-wizard.html">Form Wizard</a> </li>
                            <li><a href="form-editors.html">Editors</a> </li>
                            <li><a href="form-upload.html">File Upload</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-grid"></i><span>Tables</span> </a>
                        <ul class="ml-menu">
                            <li> <a href="normal-tables.html">Normal Tables</a> </li>
                            <li> <a href="jquery-datatable.html">Jquery Datatables</a> </li>
                            <li> <a href="editable-table.html">Editable Tables</a> </li>
                            <li> <a href="footable.html">Foo Tables</a> </li>
                            <li> <a href="table-color.html">Tables Color</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-chart"></i><span>Charts</span> </a>
                        <ul class="ml-menu">
                            <li> <a href="morris.html">Morris</a> </li>
                            <li> <a href="flot.html">Flot</a> </li>
                            <li> <a href="chartjs.html">ChartJS</a> </li>
                            <li> <a href="sparkline.html">Sparkline</a> </li>
                            <li> <a href="jquery-knob.html">Jquery Knob</a> </li>
                        </ul>
                    </li>
                    <li class="header">EXTRA COMPONENTS</li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-delicious"></i><span>Widgets</span> </a>
                        <ul class="ml-menu">
                            <li><a href="widgets-app.html">Apps Widgetse</a></li>
                            <li><a href="widgets-data.html">Data Widgetse</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-lock"></i><span>Authentication</span> </a>
                        <ul class="ml-menu">
                            <li><a href="sign-in.html">Sign In</a> </li>
                            <li><a href="sign-up.html">Sign Up</a> </li>
                            <li><a href="forgot-password.html">Forgot Password</a> </li>
                            <li><a href="404.html">Page 404</a> </li>
                            <li><a href="500.html">Page 500</a> </li>
                            <li><a href="page-offline.html">Page Offline</a> </li>
                            <li><a href="locked.html">Locked Screen</a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-copy"></i><span>Sample Pages</span> </a>
                        <ul class="ml-menu">
                            <li><a href="blank.html">Blank Page</a> </li>
                            <li> <a href="image-gallery.html">Image Gallery</a> </li>
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="timeline.html">Timeline</a></li>
                            <li><a href="pricing.html">Pricing</a></li>
                            <li><a href="invoices.html">Invoices</a></li>
                            <li><a href="search-results.html">Search Results</a></li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-map"></i><span>Maps</span> </a>
                        <ul class="ml-menu">
                            <li> <a href="google.html">Google Map</a> </li>
                            <li> <a href="yandex.html">YandexMap</a> </li>
                            <li> <a href="jvectormap.html">jVectorMap</a> </li>
                        </ul>
                    </li>
                    <li class="header">Extra</li>
                    <li>
                        <div class="progress-container progress-primary m-t-10">
                            <span class="progress-badge">Traffic this Month</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="67" aria-valuemin="0" aria-valuemax="100" style="width: 67%;">
                                    <span class="progress-value">67%</span>
                                </div>
                            </div>
                        </div>
                        <div class="progress-container progress-info">
                            <span class="progress-badge">Server Load</span>
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100" style="width: 86%;">
                                    <span class="progress-value">86%</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-pane stretchLeft active" id="Blog">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info m-b-20 p-b-15">
                            <div class="image"><a href="profile.html"><img src="assets/images/profile_av.jpg" alt="User"></a></div>
                            <div class="detail">
                                <h4>{{\Illuminate\Support\Facades\Auth::user()->name}}</h4>
                                <small>{{ \Illuminate\Support\Facades\Auth::user()->email }}</small>
                            </div>
                            <a title="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a>
                            <a title="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="instagram" href="#"><i class="zmdi zmdi-instagram"></i></a>
                        </div>
                    </li>
                    <li class="header">MAIN NAVIGATION</li>
                    <li><a href="index.html"><i class="zmdi zmdi-home"></i><span>Home</span> </a> </li>
                    <li><a href="blog-dashboard.html"><i class="zmdi zmdi-blogger"></i><span>Blog Dashboard</span> </a> </li>
                    <li> <a href="blog-post.html"><i class="zmdi zmdi-plus-circle"></i><span>New Post</span> </a> </li>
                    <li class="active open"><a href="blog-list.html"><i class="zmdi zmdi-sort-amount-desc"></i><span>Blog List</span> </a> </li>
                    <li><a href="blog-grid.html"><i class="zmdi zmdi-grid"></i><span>Blog Grid</span> </a> </li>
                    <li><a href="blog-details.html"><i class="zmdi zmdi-label-alt"></i><span>Blog Single</span> </a> </li>

                    <li class="header">CATEGORIES</li>
                    <li><a href="javascript:void(0);" class="waves-effect waves-block"><i class="zmdi zmdi-label col-green"></i><span>Web Design</span></a></li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-label col-red"></i><span>Photography</span> </a>
                        <ul class="ml-menu">
                            <li><a href="javascript:void(0);">Wild</a> </li>
                            <li><a href="javascript:void(0);">Marriage</a> </li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-label col-amber"></i><span>Technology</span> </a>
                        <ul class="ml-menu">
                            <li><a href="javascript:void(0);">UI UX Design</a> </li>
                            <li><a href="javascript:void(0);">Android</a> </li>
                            <li><a href="javascript:void(0);">iOS</a> </li>
                            <li><a href="javascript:void(0);">Wordpress</a> </li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect waves-block"><i class="zmdi zmdi-label col-purple"></i><span>Lifestyle</span></a></li>
                    <li><a href="javascript:void(0);" class="waves-effect waves-block"><i class="zmdi zmdi-label col-lime"></i><span>Sports</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</aside>